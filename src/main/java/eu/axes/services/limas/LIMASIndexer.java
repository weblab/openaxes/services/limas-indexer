package eu.axes.services.limas;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Audio;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.TemporalSegment;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.Video;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Indexer;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.core.services.indexer.IndexReturn;
import org.ow2.weblab.rdf.Value;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.python.google.common.collect.Maps;
import org.python.google.common.collect.Sets;

import ch.ebu.www.ebucore.EBUCoreAnnotator;
import eu.axes.limas.core.InformationManager;
import eu.axes.limas.core.InformationManagerFactory;
import eu.axes.limas.core.elements.Segment;
import eu.axes.limas.core.impl.DefaultInformationManager;
import eu.axes.limas.core.impl.DocumentIndexer.Normalizer;
import eu.axes.limas.core.impl.IndexException;
import eu.axes.limas.core.impl.mongodb.MongoDBIndexer;
import eu.axes.utils.AxesAnnotator;

/**
 * This service receives a weblab xml document and transfers it into key-value pairs from limas.
 *
 * At the moment, only video segments are supported.
 *
 * We roughly assume the following data structure
 *
 * <pre>
 *  Annotation
 *  Video MU (Media unit) - Whole video
 *    Annotation (Shots)
 *      ShotX-1
 *        isDelimiterOfShotUnit -> Shot-1
 *      ...
 *    Annotation (Transcripts)
 *      TranscriptX-1
 *        isDelimiterOfTranscriptUnit -> Text-1
 *      ...
 *    TemporalSegment Shot-1
 *      from
 *      to
 *    ...
 *    TemporalSegment Transcript-1
 *      from
 *      to
 *    ...
 *  Video MU - Shot-1									// shot
 *    Annotation
 *       FrameX
 *         isDelimiterOfFrameUnit -> Keyframe-1
 *       TrackX
 *       	refersTo
 *       StsX
 *          confidence
 *    TemporalSegment FrameX							 // keyframe
 *      from
 *      to                                               from = to (point in time)
 *    TrackSegment TrackX                                // track
 *      SpatioTemporalSegment StsX
 *         @timestamp
 *         coordinates
 *      ...                                              more spatio temporal segemts of this track
 *    ...                                                more tracks
 *  Image MU - Keyframe-1
 *    Annotation Keyframe-1
 *      isFrameOf -> Shot-1
 *    Annotation Seg-1
 *      refersTo -> Entity-1
 *      axes:confidence
 *    Annotation Entity-1
 *      label
 *    SpatialSegment Seg-1                               Bounding box where entity occurs
 *      coordinates
 *    ...
 *  ...                                                  more Video MU + Image MU combinations (shots / keyframes)
 *  Text MU - Text-1 (ASR)
 *    Annotation
 *      locatedAt -> TranscriptX-1                       Pointer to Temporal Segment in whole video
 *    Content                                            Actual ASR Content
 *  ...
 * </pre>
 *
 * }
 *
 * @author alyr
 * @since 2011-12-13
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Indexer")
public class LIMASIndexer implements Indexer {


	private static final String PLACES_URI = "http://axes-project.eu/v1.0/Location";


	private static final String PERSON_URI = "http://axes-project.eu/v1.0/Person";


	private static final String OBJECT_URI = "http://axes-project.eu/v1.0/Object";


	private final Log logger;


	private final DefaultInformationManager im;


	private final Normalizer normaliser;


	/**
	 * start with a particular configuration
	 *
	 * @param configfile
	 *            The path to the configuration file
	 */
	public LIMASIndexer(final String configfile) {
		super();
		this.logger = LogFactory.getLog(this.getClass());
		if (configfile == null || configfile.isEmpty()) {
			final String msg = "Configuration file is null or empty does not exist.";
			this.logger.fatal(msg);
			throw new IllegalArgumentException(msg);
		}
		final File confFile = new File(configfile);
		if (!confFile.exists() || !confFile.isFile()) {
			final String msg = "Configuration file " + confFile + " does not exist.";
			this.logger.fatal(msg);
			throw new IllegalArgumentException(msg);
		}
		InformationManager informationManager = new InformationManagerFactory().createInformationManager(confFile);
		if (!(informationManager instanceof DefaultInformationManager)) {
			final String msg = "Configuration does not enable the creation of a DefaultInformationManager but a " + informationManager.getClass().getName() + ".";
			this.logger.fatal(msg);
			throw new IllegalArgumentException(msg);
		}
		this.im = (DefaultInformationManager) informationManager;
		this.normaliser = new Normalizer((this.im).getConnection());
	}


	/**
	 * Use default configuration provided in limas.conf system property.
	 */
	public LIMASIndexer() {
		this(System.getProperty("limas.conf"));
	}


	@Override
	public IndexReturn index(final IndexArgs args) throws InvalidParameterException, UnexpectedException {
		final Document weblabDoc = LIMASIndexer.checkArgs(args);

		this.logger.info("Start indexing " + weblabDoc.getUri());
		final HashMap<String, TemporalSegment> temporalSegments = new HashMap<>();

		URI docURI = null;
		eu.axes.limas.core.elements.Document limasDoc = null;
		int shotCount = 0;
		// for all videos (shots or main video. First video is the main one)
		// Since version 1.1 there is usually only one video. Keep loop for the moment.
		String videoURL = null;
		for (final Video weblabVideo : ResourceUtil.getSelectedSubResources(weblabDoc, Video.class)) {
			if (shotCount == 0) { // first video media unit is the actual video; the others are shots
				docURI = URI.create(weblabVideo.getUri().substring("http://axes-project.eu".length()).replaceAll("#.*", ""));
				this.logger.debug(weblabVideo.getUri() + " transformed into " + docURI);
				this.logger.info("Removing document " + docURI + " before reindexing.");

				for (eu.axes.limas.core.impl.Indexer indexer : this.im.getIndexers()) {
					try {
						indexer.open();
						indexer.removeSegment(docURI);
					} catch (IndexException ie) {
						this.logger.warn("Unable to remove document " + docURI + " from index.", ie);
					} finally {
						try {
							indexer.close();
						} catch (final IndexException ie) {
							// Safe close
						}
					}
				}

				this.logger.debug("Creating document " + docURI);
				limasDoc = this.im.createDocument(docURI);
				final String tmpVideoUrl = this.indexDocument(limasDoc, weblabDoc, weblabVideo, temporalSegments);
				if (tmpVideoUrl != null && videoURL == null) {
					videoURL = tmpVideoUrl;
				}
			}
			shotCount++;
		}

		// for all transcripts
		int transcriptCount = 1;
		final Map<URI, Audio> audioSegments = Maps.newHashMap();
		for (final Audio audio : ResourceUtil.getSelectedSubResources(weblabDoc, Audio.class)) {
			final Value<URI> locatedAt = new WProcessingAnnotator(audio).readLocatedAt();
			if (locatedAt.hasValue()) {
				audioSegments.put(locatedAt.firstTypedValue(), audio);
			}
		}
		for (final Text weblabTranscript : ResourceUtil.getSelectedSubResources(weblabDoc, Text.class)) {
			this.indexSpeechSegment(limasDoc, transcriptCount++, temporalSegments, weblabTranscript, audioSegments);
		}

		// for all keyframes
		int keyframeCount = 1;
		for (final Image weblabImage : ResourceUtil.getSelectedSubResources(weblabDoc, Image.class)) {
			keyframeCount++;
			this.indexKeyFrame(limasDoc, temporalSegments, weblabImage, videoURL);
		}


		if (limasDoc != null) {
			try {
				// Call the normaliser directly for the document instead of doing it for every documents (exponential processing time).
				// This call will index inside Mongo only.
				this.normaliser.map(limasDoc);

				// Call each indexer directly
				for (final eu.axes.limas.core.impl.Indexer indexer : this.im.getIndexers()) {
					if (indexer instanceof MongoDBIndexer) {
						// Already done above.
						continue;
					}
					try {
						indexer.open();
						indexer.addDocument(limasDoc);
					} finally {
						try {
							indexer.close();
						} catch (IndexException ie) {
							// Safe close
						}
					}
				}
			} catch (final IOException | InterruptedException | IndexException e) {
				final String message = "Document " + weblabDoc.getUri() + " cannot be indexed. Please check inside Limas/Jetty logs.";
				this.logger.error(message);
				throw ExceptionFactory.createUnexpectedException(message);
			}
			this.logger.info(String.format("Document %s successfully indexed (%d transcripts, %d keyframes)", weblabDoc.getUri(), Integer.valueOf(transcriptCount), Integer.valueOf(keyframeCount)));
		} else {
			String message = "Doc " + weblabDoc.getUri() + " is not valid. Nothing to index.";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}

		return new IndexReturn();
	}


	/*
	 * Video MU (Media unit) - Whole video
	 * Annotation (Shots)
	 * ShotX-1
	 * isDelimiterOfShotUnit -> Shot-1
	 * ...
	 * Annotation (Transcripts)
	 * TranscriptX-1
	 * isDelimiterOfTranscriptUnit -> Text-1
	 * ...
	 * TemporalSegment ShotX-1
	 * from
	 * to
	 * ...
	 * TemporalSegment TranscriptX-1
	 * from
	 * to
	 * ...
	 */
	private String indexDocument(final eu.axes.limas.core.elements.Document limasDoc, final Document weblabDoc, final MediaUnit weblabVideoMU,
			final Map<String, org.ow2.weblab.core.model.TemporalSegment> temporalSegments) {
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(weblabDoc);
		final DublinCoreAnnotator dca2 = new DublinCoreAnnotator(weblabVideoMU);
		final WProcessingAnnotator wpaDoc = new WProcessingAnnotator(weblabDoc);
		final WProcessingAnnotator wpaSeg = new WProcessingAnnotator(weblabVideoMU);
		final AxesAnnotator axesDocMU = new AxesAnnotator(weblabVideoMU);
		final EBUCoreAnnotator ebu = new EBUCoreAnnotator(weblabDoc);

		String videoURL = null;
		if ((wpaSeg.readExposedAs() != null) && wpaSeg.readExposedAs().hasValue()) {
			videoURL = wpaSeg.readExposedAs().firstTypedValue();
		}

		this.addCommonInfo(limasDoc, weblabVideoMU, wpaSeg);

		LIMASIndexer.assignIfNotNull(limasDoc, Segment.SOURCE, dca.readSource());
		LIMASIndexer.assignIfNotNull(limasDoc, Segment.CREATOR, wpaSeg.readProducedBy());
		LIMASIndexer.assignIfNotNull(limasDoc, Segment.SIZE, wpaDoc.readOriginalFileSize());
		LIMASIndexer.assignIfNotNull(limasDoc, Segment.ORIGINAL_FILENAME, wpaDoc.readOriginalFileName());

		Set<String> vals = new HashSet<>(dca.readTitle().getValues());
		vals.addAll(ebu.getMainTitles());
		LIMASIndexer.assignSet(limasDoc, Segment.TITLE, vals);
		vals.clear();

		LIMASIndexer.assignIfNotNull(limasDoc, Segment.LANGUAGE, dca.readLanguage());

		vals.addAll(dca.readDescription().getValues());
		LIMASIndexer.assignSet(limasDoc, Segment.DESCRIPTION, vals);
		vals.clear();

		vals = ebu.getSummaries();
		LIMASIndexer.assignSet(limasDoc, Segment.SUMMARY, vals);
		vals.clear();

		vals = ebu.getSubjects();
		LIMASIndexer.assignSet(limasDoc, Segment.SUBJECT, vals);
		vals.clear();

		vals = ebu.getKeywords();
		LIMASIndexer.assignSet(limasDoc, Segment.KEYWORDS, vals);
		vals.clear();

		vals.addAll(ebu.getGenres());
		LIMASIndexer.assignSet(limasDoc, Segment.GENRE, vals);
		vals.clear();

		vals = ebu.getColourSpaces();
		LIMASIndexer.assignSet(limasDoc, Segment.COLORSPACE, vals);
		vals.clear();

		try {
			this.readEbuPublicationDates(ebu.getPubliEvents(), vals);
			if (!vals.isEmpty()) {
				final String val = Collections.max(vals);
				limasDoc.put(Segment.PUBLICATIONDATE, val);
			} else {
				limasDoc.put(Segment.PUBLICATIONDATE, "");
			}
			vals.clear();

			LIMASIndexer.readEbuStrings(ebu.readPublished(), vals);
			LIMASIndexer.assignSet(limasDoc, Segment.PUBLICATIONINFO, vals);
		} catch (final IllegalArgumentException iae) {
			this.logger.error("Unable to get publication date or publication channel.", iae);
		}
		vals.clear();

		LIMASIndexer.readEbuContributors(ebu.getContributors(), vals);
		LIMASIndexer.assignSet(limasDoc, Segment.CONTRIBUTORS, vals);
		vals.clear();

		final Map<String, Set<String>> rights = ebu.getRights();
		vals.addAll(rights.keySet());
		LIMASIndexer.assignSet(limasDoc, Segment.RIGHTS, vals);
		vals.clear();

		LIMASIndexer.readEntities(wpaDoc, LIMASIndexer.PLACES_URI, vals);
		LIMASIndexer.assignSet(limasDoc, Segment.PLACES, vals);
		vals.clear();

		LIMASIndexer.readEntities(wpaDoc, LIMASIndexer.PERSON_URI, vals);
		LIMASIndexer.assignSet(limasDoc, Segment.PERSONS, vals);
		vals.clear();

		LIMASIndexer.readEntities(wpaDoc, LIMASIndexer.OBJECT_URI, vals);
		LIMASIndexer.assignSet(limasDoc, Segment.OBJECTS, vals);
		vals.clear();

		LIMASIndexer.readEntities(wpaDoc, null, vals);
		LIMASIndexer.assignSet(limasDoc, Segment.ENTITIES, vals);
		vals.clear();

		/*
		 * TECHNICAL DATA
		 */

		// Native Content
		int i = 0;
		for (final URI nativeContent : wpaSeg.readNativeContent()) {
			dca2.startInnerAnnotatorOn(nativeContent);
			final String nativeContentType = dca2.readFormat().firstTypedValue();
			dca2.endInnerAnnotator();
			limasDoc.put(Segment.NATIVE_CONTENT + "_" + i, nativeContent);
			limasDoc.put(Segment.NATIVE_CONTENT_TYPE + "_" + i, nativeContentType);
			i++;
		}
		limasDoc.put(Segment.NATIVE_CONTENT_COUNT, i + "");


		// Normalized Content (conversions)
		i = 0;
		final Iterator<String> x = wpaSeg.readExposedAs().iterator();
		for (final URI normalizedContent : wpaSeg.readNormalisedContent()) {
			if (!x.hasNext()) {
				this.logger.warn("Not enough exposed as entries");
				break;
			}
			final String exposed = x.next();

			dca2.startInnerAnnotatorOn(normalizedContent);
			final String normalizedContentType = dca2.readFormat().firstTypedValue();
			dca2.endInnerAnnotator();
			limasDoc.put(Segment.NORMALIZED_CONTENT + "_" + i, normalizedContent);
			limasDoc.put(Segment.NORMALIZED_CONTENT_TYPE + "_" + i, normalizedContentType);
			if (exposed != null) {
				limasDoc.put(Segment.NORMALIZED_CONTENT_EXPOSEDAS + "_" + i, exposed);
				if (exposed.endsWith(".webm")) {
					videoURL = exposed;
				}
			} else {
				this.logger.error("Exposed as for normalized content " + normalizedContent + " is null");
			}
			i++;
		}
		limasDoc.put(Segment.NORMALIZED_CONTENT_COUNT, i + "");
		limasDoc.put(Segment.URL, videoURL);

		// Temporal Segments
		final List<org.ow2.weblab.core.model.Segment> weblabSegments = weblabVideoMU.getSegment();
		for (final org.ow2.weblab.core.model.Segment weblabSeg : weblabSegments) {

			if (weblabSeg instanceof org.ow2.weblab.core.model.TemporalSegment) {
				final TemporalSegment temporalinfo = (TemporalSegment) weblabSeg;
				temporalSegments.put(temporalinfo.getUri(), temporalinfo);

				axesDocMU.startInnerAnnotatorOn(URI.create(temporalinfo.getUri()));
				wpaSeg.startInnerAnnotatorOn(URI.create(weblabSeg.getUri()));

				final URI utype = LIMASIndexer.readSafe(wpaSeg.readType(), null);
				String type = null;
				if (utype != null) {
					type = utype.toString();
				}
				if ("http://axes-project.eu/classes#ShotSegment".equals(type)) {
					final String shotId = LIMASIndexer.readSafe(axesDocMU.readShotId(), null);
					if (shotId != null) {
						temporalSegments.put(shotId, temporalinfo);
					}
				}
				wpaSeg.endInnerAnnotator();
				axesDocMU.endInnerAnnotator();
			} else {
				this.logger.warn(String.format("Unknown segment: %s (%s)\n", weblabSeg.getUri(), weblabSeg.getClass().getName()));
				continue;
			}
			wpaSeg.endInnerAnnotator();
		}

		return videoURL;
	}


	private static void assignSet(final Segment s, final String field, final Set<String> vals) {
		s.put(field, LIMASIndexer.setToString(vals, "; "));
	}


	private static void readEntities(final WProcessingAnnotator wpaDoc, final String type, final Set<String> set) {
		for (final URI refersTo : wpaDoc.readRefersTo()) {
			wpaDoc.startInnerAnnotatorOn(refersTo);
			final Value<URI> typeValue = wpaDoc.readType();
			if ((type == null) || (typeValue.hasValue() && type.equals(typeValue.firstTypedValue().toString()))) {
				set.add(wpaDoc.readLabel().firstTypedValue());
			}
			wpaDoc.endInnerAnnotator();
		}
	}


	private static <T> String setToString(final Set<T> set, final String delim) {
		final StringBuilder builder = new StringBuilder();
		for (final T entry : set) {
			// skip null entries
			if ((entry == null) || (entry.toString() == null)) {
				continue;
			}
			if (builder.length() > 0) {
				builder.append(delim);
			}
			String e = entry.toString();
			e = e.replaceAll(delim, "");
			builder.append(e);
		}
		return builder.toString();
	}


	private void readEbuPublicationDates(final Map<Date, Set<String>> val, final Set<String> results) {
		final DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		for (final Date event : val.keySet()) {
			try {
				results.add(format.format(event));
			} catch (final NullPointerException npe) {
				this.logger.error(npe);
			}
		}
	}


	private static void readEbuContributors(final Map<String, Set<String>> map, final Set<String> results) {
		for (final Entry<String, Set<String>> kv : map.entrySet()) {
			kv.getKey();
			final Set<String> actors = kv.getValue();
			for (final String actor : actors) {
				results.add(actor);
			}
		}
	}


	private static <T> void readEbuStrings(final Value<T> val, final Set<String> results) {
		if (val.hasValue()) {
			for (final T keyword : val.getValues()) {
				results.add(keyword.toString());
			}
		}
	}


	/*
	 * Add common info of a segment
	 */
	private void addCommonInfo(final Segment limasSeg, final MediaUnit mu, final WProcessingAnnotator wpaSeg) {
		LIMASIndexer.assignIfNotNull(limasSeg, Segment.CREATOR, wpaSeg.readProducedBy());
		if (mu == null) {
			this.logger.error("Mu null");
		} else if (limasSeg == null) {
			this.logger.error("Limas seg null");
		} else {
			limasSeg.put(Segment.AXES_URI, mu.getUri());
		}
	}



	/*
	 * Image MU - Keyframe-1
	 * Annotation Keyframe-1
	 * isFrameOf -> Shot-1
	 * Annotation Seg-1
	 * refersTo -> Entity-1
	 * axes:confidence
	 * Annotation Entity-1
	 * label
	 * SpatialSegment Seg-1 Bounding box where entity occurs
	 * coordinates
	 * ...
	 */
	private void indexKeyFrame(final eu.axes.limas.core.elements.Document limasDoc, final Map<String, TemporalSegment> temporalSegments, final Image weblabMU, final String videoURL) {
		final WProcessingAnnotator wpaMU = new WProcessingAnnotator(weblabMU);
		final DublinCoreAnnotator dc = new DublinCoreAnnotator(weblabMU);
		final AxesAnnotator axesAnnot = new AxesAnnotator(weblabMU);

		final URI locatedAt = LIMASIndexer.readSafe(wpaMU.readLocatedAt(), null);
		if (locatedAt == null) {
			this.logger.warn("No locatedAt statement for " + weblabMU.getUri());
			return;
		}
		// build IDs
		final String shotId = LIMASIndexer.readSafe(axesAnnot.readShotId(), null);
		final String frameId = LIMASIndexer.readSafe(axesAnnot.readFrameId(), null);
		final URI shotURI = URI.create(limasDoc.getURI() + "/" + shotId);
		final URI keyframeURI = URI.create(shotURI + "/" + frameId);

		final TemporalSegment frameSegment = temporalSegments.get(locatedAt + "");
		if (frameSegment == null) {
			this.logger.warn("No temporal segment defined for " + weblabMU.getUri() + " located at " + locatedAt);
			return;
		}
		final TemporalSegment shotSegment = temporalSegments.get(shotId);
		if (shotSegment == null) {
			this.logger.warn("No temporal segment defined for shotId " + shotId + " located at " + locatedAt);
			return;
		}

		/*
		 * SHOT. Reuse existing one or create a new one.
		 */
		Segment limasShot = null;
		List<Segment> shots = limasDoc.getTypeMap().get(Segment.Type.SHOT.type);
		if (shots != null) {
			for (Segment existingShot : shots) {
				if (existingShot.getURI().toASCIIString().equals(shotURI.toASCIIString())) {
					limasShot = existingShot;
					break;
				}
			}
		}

		String keyframeURL = LIMASIndexer.readSafe(wpaMU.readExposedAs(), null);
		final String thumbnailURL = LIMASIndexer.readSafe(wpaMU.readExposedAsThumbnail(), "");
		// hack to get the right keyframe name
		if (keyframeURL == null) {
			keyframeURL = thumbnailURL.replaceAll("-thumb", "");
		}

		if (limasShot == null) {
			limasShot = this.im.createSegment(shotURI);
			this.logger.debug("Create Shot segment " + shotURI.toASCIIString());
			this.addCommonInfo(limasShot, weblabMU, wpaMU);
			LIMASIndexer.addTemporalInfo(limasShot, shotSegment);
			
			limasShot.put(Segment.KEYFRAME_URL, keyframeURL);
			limasShot.put(Segment.THUMBNAIL_URL, thumbnailURL);
			limasShot.put(Segment.URL, videoURL);
			
			limasDoc.addSegment(limasShot);
		}

		/*
		 * KEYFRAME
		 */
		final Segment limasKeyframe = this.im.createSegment(keyframeURI);
		this.logger.debug("Create Keyframe segment " + keyframeURI.toASCIIString());
		this.addCommonInfo(limasKeyframe, weblabMU, wpaMU);
		LIMASIndexer.addTemporalInfo(limasKeyframe, frameSegment);
		final URI nativeContent = wpaMU.readNativeContent().firstTypedValue();
		dc.startInnerAnnotatorOn(nativeContent);
		final String nativeContentType = dc.readFormat().firstTypedValue();
		dc.endInnerAnnotator();
		limasKeyframe.put(Segment.NATIVE_CONTENT, nativeContent);
		limasKeyframe.put(Segment.NATIVE_CONTENT_TYPE, nativeContentType);
		limasKeyframe.put(Segment.KEYFRAME_URL, keyframeURL);
		limasKeyframe.put(Segment.THUMBNAIL_URL, thumbnailURL);

		limasDoc.addSegment(limasKeyframe);
	}


	/*
	 * ... more Video MU + Image MU combinations (shots / keyframes)
	 * Text MU - Text-1 (ASR)
	 * Annotation
	 * locatedAt -> TranscriptX-1 Pointer to Temporal Segment in whole video
	 * Content Actual ASR Content
	 */
	private void indexSpeechSegment(final eu.axes.limas.core.elements.Document limasDoc, final int count, final HashMap<String, TemporalSegment> temporalSegments, final Text textcontent,
			final Map<URI, Audio> audioSegments) {
		final WProcessingAnnotator wpaMU = new WProcessingAnnotator(textcontent);
		final URI locatedAt = wpaMU.readLocatedAt().firstTypedValue();
		if (locatedAt == null) {
			this.logger.warn("No locatedAt value for " + textcontent.getUri());
			return;
		}
		final org.ow2.weblab.core.model.TemporalSegment temporalSegment = temporalSegments.get(locatedAt + "");
		if (temporalSegment == null) {
			this.logger.warn("No temporal segment defined for " + textcontent.getUri() + " located at " + locatedAt);
			return;
		}
		final Audio audio = audioSegments.get(locatedAt);

		final URI uri = URI.create(String.format("%s/t%03d", limasDoc.getURI(), Integer.valueOf(count)));
		final Segment limasSpeech = this.im.createSegment(uri);
		this.logger.debug("Create Speech segment " + uri.toASCIIString() + " for Text " + textcontent.getUri() + ".");
		this.addCommonInfo(limasSpeech, textcontent, wpaMU);
		LIMASIndexer.addTemporalInfo(limasSpeech, temporalSegment);
		limasSpeech.put(Segment.ASR, textcontent.getContent());

		if (audio != null) {
			final WProcessingAnnotator wpaAudio = new WProcessingAnnotator(audio);
			final Value<URI> refersTo = wpaAudio.readRefersTo();
			if (refersTo.hasValue()) {
				limasSpeech.put(Segment.SPEAKERID, refersTo.firstTypedValue().toString());
			}
		}

		final Set<String> places = Sets.newTreeSet();
		final Set<String> persons = Sets.newTreeSet();
		final Set<String> objects = Sets.newTreeSet();
		LIMASIndexer.readEntities(wpaMU, LIMASIndexer.PLACES_URI, places);
		LIMASIndexer.readEntities(wpaMU, LIMASIndexer.PERSON_URI, persons);
		LIMASIndexer.readEntities(wpaMU, LIMASIndexer.OBJECT_URI, objects);

		final List<org.ow2.weblab.core.model.Segment> weblabSegments = textcontent.getSegment();
		for (final org.ow2.weblab.core.model.Segment weblabSeg : weblabSegments) {
			final URI seguri = URI.create(weblabSeg.getUri());
			wpaMU.startInnerAnnotatorOn(seguri);
			LIMASIndexer.readEntities(wpaMU, LIMASIndexer.PLACES_URI, places);
			LIMASIndexer.readEntities(wpaMU, LIMASIndexer.PERSON_URI, persons);
			LIMASIndexer.readEntities(wpaMU, LIMASIndexer.OBJECT_URI, objects);
			wpaMU.endInnerAnnotator();
		}
		LIMASIndexer.setAllEntities(limasSpeech, places, persons, objects);

		limasDoc.addSegment(limasSpeech);
	}


	private static void setAllEntities(final Segment s, final Set<String> places, final Set<String> persons, final Set<String> objects) {
		if (!places.isEmpty()) {
			s.put(Segment.PLACES, LIMASIndexer.setToString(places, "; "));
		}
		if (!persons.isEmpty()) {
			s.put(Segment.PERSONS, LIMASIndexer.setToString(persons, "; "));
		}
		if (!objects.isEmpty()) {
			s.put(Segment.OBJECTS, LIMASIndexer.setToString(objects, "; "));
		}

		final Set<String> entities = Sets.newTreeSet();
		entities.addAll(places);
		entities.addAll(persons);
		entities.addAll(objects);
		if (!entities.isEmpty()) {
			s.put(Segment.ENTITIES, LIMASIndexer.setToString(entities, "; "));
		}
	}


	private static void addTemporalInfo(final Segment limasSpeech, final TemporalSegment temporalSegment) {
		limasSpeech.put(Segment.START, Integer.valueOf(temporalSegment.getStart()));
		limasSpeech.put(Segment.STOP, Integer.valueOf(temporalSegment.getEnd()));
	}


	private static <T> T readSafe(final Value<T> v, final T def) {
		if ((v == null) || !v.hasValue()) {
			return def;
		}
		return v.firstTypedValue();
	}


	private static <T> void assignIfNotNull(final Segment s, final String fieldName, final Value<T> v) {
		final T x = LIMASIndexer.readSafe(v, null);
		if (x == null) {
			return;
		}
		s.put(fieldName, v.toString());
	}


	private static Document checkArgs(final IndexArgs args) throws InvalidParameterException {
		if (args == null) {
			final String msg = "ProcessArgs was null.";
			LogFactory.getLog(LIMASIndexer.class).error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}
		final Resource res = args.getResource();
		if (res == null) {
			final String msg = "Resource in ProcessArgs was null.";
			LogFactory.getLog(LIMASIndexer.class).error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}

		if (!(res instanceof Document)) {
			final String msg = "Resource " + res.getUri() + "is not a document.";
			LogFactory.getLog(LIMASIndexer.class).error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}

		Document doc = (Document) res;
		final List<MediaUnit> innerMus = doc.getMediaUnit();
		if (innerMus.isEmpty()) {
			final String msg = "Not a single MediaUnit inside document " + res.getUri() + ".";
			LogFactory.getLog(LIMASIndexer.class).error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}
		if (!(innerMus.get(0) instanceof Video)) {
			final String msg = "First MediaUnit inside document " + res.getUri() + " is not a Video while it is mandatory.";
			LogFactory.getLog(LIMASIndexer.class).error(msg);
			throw ExceptionFactory.createInvalidParameterException(msg);
		}
		return doc;

	}

}
