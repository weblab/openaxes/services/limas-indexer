#
# This script is intended to be called from inside InformationManagerFactory to configure instances of the information manager.
# This is not intended to be called directly!
#

import os
from eu.axes.limas.core.elements import Segment

#
# Constants
#
DB_HOST				= 'localhost'
DB_PORT				= 27017
COLLECTION			= 'cOpenAXES'
LIMAS_DATA_DIR		= 'target/limas'
COLLECTIONS_PATH	= 'target/collections'
LANGUAGE			= 'en'
LUCENE_INDICES_PATH	= os.path.join('target/indices/lucene', COLLECTION)
URL_PREFIX			= 'http://localhost/'
DB_PREFIX			= COLLECTION + '_'


# Shot level search fields                                                                                                                                   
ASR_FIELDS =  [
    Segment.ASR,		None,
    Segment.ENTITIES,	None,
]


# Video level search fields                   
METADATA_FIELDS = [
    Segment.TITLE,				'title',
    Segment.KEYWORDS,			'keywords',
    Segment.SUMMARY,			'summary',
    Segment.SUBJECT,			None,
    Segment.UPLOADER,			None,
    Segment.PERSONS,			'persons',
    Segment.OBJECTS,			'objects',
    Segment.PLACES,				'places',
    Segment.ENTITIES,			'entities',
    Segment.PUBLICATIONDATE,	'date'
]


# Simple mkDirs method
def mkDirs(path):
    import os
    try: 
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise


# Create needed directories
mkDirs(LUCENE_INDICES_PATH)



from eu.axes.limas.core.impl    import Normalizer
Normalizer.createNormalizer(LANGUAGE)


'''
(source, impl, id, url, database, humanReadable, searchURL) 
'''
services = [
    ('#meta',		'LUCENE',			'Meta',		COLLECTION,	'metadata',		os.path.join(LUCENE_INDICES_PATH,'meta_lucene.idx'),	METADATA_FIELDS),
	('#speech',		'LUCENE',			'Speech',	COLLECTION,	'speech',		os.path.join(LUCENE_INDICES_PATH, 'asr_lucene.idx'),	ASR_FIELDS),
	('#entity',		'MONGODB_ENTITY',	'Entity',	COLLECTION,	'entity',		'XX',										None),
]



import types 
import sys

def instantiate(manager, factory, DB_HOST, DB_PORT, COLLECTION, INDEX_PATH, URL_PREFIX, ASR_FIELDS, METADATA_FIELDS, services):
    # ==========================================================
    # General Instance Configuration
    #
    from eu.axes.limas.core.impl.mongodb    import MongoDBConnection
    hconf = MongoDBConnection(DB_HOST, DB_PORT, COLLECTION)
    hconf.register(manager)

    from eu.axes.limas.core.impl.entityclassifier import ECQueryRewriter
    from eu.axes.limas.core.impl import Normalizer
    manager.queryParser = ECQueryRewriter(Normalizer.inst().getLang())

    from eu.axes.limas.core.impl.entityclassifier    import LingPipeClassifier
    from eu.axes.limas.core.impl    import Normalizer
    manager.queryParser = LingPipeClassifier(manager.entityStore)

    from eu.axes.limas.core.impl    import URIHandlerStandard
    uriHandler = URIHandlerStandard(URL_PREFIX)
    manager.setUriHandler(uriHandler)

    # ==========================================================
    # Indexing Components
    #
    # Add ASR linker
    from eu.axes.limas.core.impl.linking import ASRLinker
    linker = ASRLinker(manager.entityStore, manager)
    manager.setLinker(linker)

    for service, impl, idS, dataset, humanReadable, searchURL, contentInfo in services:
        if impl == 'LUCENE': 
            mkDirs(searchURL)
            from eu.axes.limas.core.impl.lucene    import LuceneIndexer
            indexer = LuceneIndexer(manager.entityStore, searchURL, contentInfo)
            if contentInfo == ASR_FIELDS:
                indexer.setRequiredSegmentType('s')
            else:
                indexer.setRequiredSegmentType('v')
            manager.addIndexer(indexer)

    #Add entity recommenders
    #from eu.axes.limas.core.impl.lucene import LuceneEntityIndexer
    #mkDirs(INDEX_PATH + '/entity_lucene.idx')
    #indexer = LuceneEntityIndexer(manager.entityStore, INDEX_PATH + '/entity_lucene.idx')
    #manager.addIndexer(indexer);


    #from eu.axes.limas.core.impl.lucene import LuceneEntityRecommender
    #recommender = LuceneEntityRecommender(manager.entityStore, INDEX_PATH + '/entity_lucene.idx')
    manager.addEntityRecommender(manager.queryParser)


    # ==========================================================
    # Search Components
    #

    # Instantiate AdvancedFusion searcher

    from eu.axes.limas.core.impl.fusion    import AdvancedFusionSearcherFactory
    factory = AdvancedFusionSearcherFactory(manager.entityStore)
    factory.setRequiredSegmentType('s|v')
    factory.setDebug(False)
    factory.setDoCache(False)
    factory.setWindowSize(1)
    factory.setRecommender(manager.queryParser)
    manager.addSearcherFactory(factory)

    for service, impl, idS, dataset, humanReadable, searchURL, contentInfo in services:
        if impl == 'LUCENE':
            from eu.axes.limas.core.impl.lucene    import LuceneKSFactory
            ksf = LuceneKSFactory(manager.entityStore, searchURL)
        elif impl == 'ENTITY':
            from eu.axes.limas.core.impl.lucene    import LuceneEntityOccurrences
            leo = LuceneEntityOccurrences(manager.entityStore, searchURL)
            ksf = leo.getKSFactory()
            manager.addIndexer(leo.getIndexer())
            factory.addSubComponent(leo)
        elif impl == 'API' and service.endswith('-i'):
            from eu.axes.limas.core.impl.similarity import SimilarityKSFactory
            ksf = SimilarityKSFactory(manager.entityStore)
            ksf.datasetName = dataset
        elif impl == 'MONGODB_ENTITY':
            from eu.axes.limas.core.impl.mongodb    import MongoDBEntityKSFactory
            ksf = MongoDBEntityKSFactory(manager.entityStore)
        elif impl == 'API2' and service.endswith('-t'):
            from eu.axes.limas.core.impl.otf import OTFKSFactory
            ksf = OTFKSFactory(manager.entityStore)
            ksf.datasetName = dataset
            ksf.resultsToRetrieve = 1000
        elif impl == 'BackendAPI' and service.endswith('-t'):
            from eu.axes.limas.core.impl.otf.backend    import BackendKSFactory
            ksf = BackendKSFactory(manager.entityStore)
            ksf.datasetName = dataset
            ksf.resultsToRetrieve = 1000    
        elif impl == 'API' and service.endswith('-t'):
            from eu.axes.limas.core.impl.similarity import ObjectLearnerKSFactory
            ksf = ObjectLearnerKSFactory(manager.entityStore)
            ksf.datasetName = dataset
        elif impl == 'SOLR' and service.endswith('-i'):
            from eu.axes.limas.core.impl.similarity import InriaSimilarityKSFactory
            ksf = InriaSimilarityKSFactory(manager.entityStore, searchURL)
        else:
            print "Unknown impl typ " + impl
            sys.exit(1)

        if impl == 'BackendAPI': 
            host = searchURL[0:searchURL.index(":")]
            port = int(searchURL[searchURL.index(":")+1:])
            ksf.config.setBackendHostName(host)
            ksf.config.setBackendPort(port)
            if not isinstance(contentInfo, types.FunctionType):        
                ksf.config.setBasePath(contentInfo)
    
        if contentInfo != None and isinstance(contentInfo, types.FunctionType):
            contentInfo(ksf)

        ksf.ID = idS
        ksf.humanReadable = humanReadable

        # add to the right knowledgesource
        if   service == "#meta":       factory.addMetaFactory(ksf)
        elif service == "#speech":     factory.addSpeechFactory(ksf)
        elif service == "#entity":     factory.addEntityFactory(ksf)
        elif service == "#category-i": factory.addCategoryIFactory(ksf)
        elif service == "#category-t": factory.addCategoryTFactory(ksf)
        elif service == "#instance-i": factory.addInstanceIFactory(ksf)
        elif service == "#instance-t": factory.addInstanceTFactory(ksf)
        elif service == "#face-i":     factory.addFaceIFactory(ksf)
        elif service == "#face-t":     factory.addFaceTFactory(ksf)
        else: print "error"

        if service != "#entity": factory.addSubComponent(ksf)

instantiate(manager, factory, DB_HOST, DB_PORT, COLLECTION, LUCENE_INDICES_PATH, URL_PREFIX, ASR_FIELDS, METADATA_FIELDS, services)
