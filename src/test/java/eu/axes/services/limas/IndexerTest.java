package eu.axes.services.limas;

import java.io.File;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.services.indexer.IndexArgs;
import org.ow2.weblab.core.services.indexer.IndexReturn;

/**
 * @author ymombrun
 */
public class IndexerTest {


	/**
	 * Need a mongo started on your local computer.
	 */
	@Test
	@Ignore
	public void testIndexing() throws Exception {
		System.setProperty("limas.home", "target/limas");

		LIMASIndexer limasIndexer = new LIMASIndexer("src/test/resources/cOpenAXES.py");

		IndexArgs ia = new IndexArgs();
		ia.setResource(new WebLabMarshaller(true).unmarshal(new File("src/test/resources/vthe_daily_2011_09_26_cnn_short.xml"), Document.class));

		IndexReturn ir = limasIndexer.index(ia);
		Assert.assertNotNull(ir);
	}

}
